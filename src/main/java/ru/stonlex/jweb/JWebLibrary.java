package ru.stonlex.jweb;

import lombok.Getter;
import ru.stonlex.jweb.common.scheduler.SchedulerManager;
import ru.stonlex.jweb.http.JHttpConnection;
import ru.stonlex.jweb.web.WebFactory;

import java.net.URL;

public final class JWebLibrary {

    @Getter
    private static final WebFactory webFactory = new WebFactory();

    @Getter
    private static final SchedulerManager schedulers = new SchedulerManager();


    /**
     * test bootstrap
     */
    public static void main(String[] args) throws Exception {
        URL url = new URL("https://googlefds.codsfm/");

        JHttpConnection httpConnection = new JHttpConnection(url);
        httpConnection.connect();
    }

}
