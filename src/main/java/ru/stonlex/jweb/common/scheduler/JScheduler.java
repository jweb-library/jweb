package ru.stonlex.jweb.common.scheduler;

import lombok.Getter;
import lombok.RequiredArgsConstructor;
import org.apache.commons.lang3.RandomStringUtils;
import ru.stonlex.jweb.JWebLibrary;

import java.util.concurrent.TimeUnit;

@RequiredArgsConstructor
public abstract class JScheduler implements Runnable {

    @Getter
    private final String identifier;

    /**
     * Если инлификатор в конструкторе не
     * указан, то он будет установлен на рандомные символы,
     * благодаря библиотеке org.apache.commons.lang3
     */
    public JScheduler() {
        this( RandomStringUtils.randomAlphabetic(64) );
    }


    /**
     * Отмена и закрытие потока
     */
    public void cancel() {
        JWebLibrary.getSchedulers().cancelScheduler(identifier);
    }

    /**
     * Запустить асинхронный поток
     */
    public void runAsync() {
        JWebLibrary.getSchedulers().runAsync(this);
    }

    /**
     * Запустить поток через определенное
     * количество времени
     *
     * @param delay - время
     * @param timeUnit - единица времени
     */
    public void runLater(long delay, TimeUnit timeUnit) {
        JWebLibrary.getSchedulers().runLater(identifier, this, delay, timeUnit);
    }

    /**
     * Запустить цикличный поток через
     * определенное количество времени
     *
     * @param delay - время
     * @param period - период цикличного воспроизведения
     * @param timeUnit - единица времени
     */
    public void runTimer(long delay, long period, TimeUnit timeUnit) {
        JWebLibrary.getSchedulers().runTimer(identifier, this, delay, period, timeUnit);
    }

}
