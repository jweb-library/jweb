package ru.stonlex.jweb.common.utility;

import com.google.gson.Gson;
import lombok.Getter;
import lombok.experimental.UtilityClass;

@UtilityClass
public class JsonUtil {

    @Getter
    private final Gson gson = new Gson();


    /**
     * Создать JSON-сообщение из объекта
     *
     * @param objectToJson - объект
     */
    public String toJson(Object objectToJson) {
        return gson.toJson(objectToJson);
    }

    /**
     * Преобразить JSON-сообщение обратно в объект
     *
     * @param jsonText - json сообщение
     * @param objectType - класс (тип) объекта, в который преобразить
     */
    public <T> T fromJson(String jsonText, Class<T> objectType) {
        return gson.fromJson(jsonText, objectType);
    }

}
