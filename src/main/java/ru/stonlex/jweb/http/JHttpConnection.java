package ru.stonlex.jweb.http;

import lombok.Getter;
import lombok.RequiredArgsConstructor;
import ru.stonlex.jweb.http.exception.JHttpConnectException;
import ru.stonlex.jweb.http.response.JHttpResponseHandler;

import java.io.IOException;
import java.net.URL;
import java.net.URLConnection;

@Getter
@RequiredArgsConstructor
public class JHttpConnection {

    private final URL url;

    private boolean connected;


    private JHttpResponseHandler responseHandler;

    private URLConnection connection;


    /**
     * Получить сообщение Response из запроса
     *
     * @param request - запрос
     */
    public String getResponseMessage(String request) throws IOException {
        return responseHandler.getResponseMessage(request, true);
    }

    /**
     * Получить код Response из запроса
     *
     * @param request - запрос
     */
    public int getResponseCode(String request) throws IOException {
        return responseHandler.getResponseCode(request, true);
    }

    /**
     * Проверить наличие библиотеки на подключенном сайте
     */
    public boolean hasLibrary() throws IOException {
        return getResponseMessage("has.library").equals("SUCCESS");
    }

    /**
     * Подключиться к URL
     */
    public void connect() throws IOException {
        URLConnection urlConnection = url.openConnection();

        try {
            urlConnection.getInputStream();
        } catch (Exception ex) {
            throw new JHttpConnectException("unknown host: " + url.toString());
        }

        this.connection = urlConnection;

        this.connected = true;
        this.responseHandler = new JHttpResponseHandler(this);
    }

}
