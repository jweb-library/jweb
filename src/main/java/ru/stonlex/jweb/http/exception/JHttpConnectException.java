package ru.stonlex.jweb.http.exception;

public class JHttpConnectException extends JHttpException {

    public JHttpConnectException(String errorMessage) {
        super(errorMessage);
    }

}
