package ru.stonlex.jweb.http.exception;

public class JHttpException extends RuntimeException {

    public JHttpException(String errorMessage) {
        super(errorMessage);
    }
}
