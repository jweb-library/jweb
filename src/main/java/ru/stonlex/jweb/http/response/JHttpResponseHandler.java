package ru.stonlex.jweb.http.response;

import lombok.Getter;
import lombok.RequiredArgsConstructor;
import ru.stonlex.jweb.http.JHttpConnection;

import java.io.IOException;
import java.net.HttpURLConnection;
import java.net.URL;

@RequiredArgsConstructor
@Getter
public class JHttpResponseHandler {

    private final JHttpConnection httpConnection;

    /**
     * Получить Response одклбчение
     *
     * @param request - запрос
     * @param useConnection - использовать ли url как префикс при запросе
     */
    private HttpURLConnection getResponseConnection(String request, boolean useConnection) throws IOException {
        String urlString = request;

        if (useConnection) {
            urlString = httpConnection.getUrl().getHost()
                    .concat("/")
                    .concat(urlString);
        }

        URL url = new URL(urlString);
        HttpURLConnection urlConnection = ((HttpURLConnection) url.openConnection());

        urlConnection.setRequestMethod("GET");
        urlConnection.connect();

        return urlConnection;
    }

    /**
     * Получить код Response из запроса
     *
     * @param request - запрос
     * @param useConnection - использовать ли url как префикс при запросе
     */
    public int getResponseCode(String request, boolean useConnection) throws IOException {
        return getResponseConnection(request, useConnection).getResponseCode();
    }

    /**
     * Получить сообщение Response из запроса
     *
     * @param request - запрос
     * @param useConnection - использовать ли url как префикс при запросе
     */
    public String getResponseMessage(String request, boolean useConnection) throws IOException {
        return getResponseConnection(request, useConnection).getResponseMessage();
    }

}
