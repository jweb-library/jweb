package ru.stonlex.jweb.web.adapter.html;

import lombok.Getter;
import lombok.RequiredArgsConstructor;

import java.util.List;

@Getter
@RequiredArgsConstructor
public class HtmlTag {

    private final String tagName;

    private final List<HtmlTag> parentTagList;
}
