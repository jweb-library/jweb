package ru.stonlex.jweb.web.adapter.script;

import lombok.Getter;

import java.io.PrintStream;
import java.util.logging.Level;
import java.util.logging.Logger;

@Getter
public class ScriptConsole {

    private final Logger logger = Logger.getGlobal();
    private final PrintStream printStream = System.out;


    /**
     * Получить кол-во используемой памяти
     */
    public final long memory() {
        return Runtime.getRuntime().totalMemory();
    }

    /**
     * Очистить консоль
     */
    public void clear() {
        printStream.flush();
    }

    /**
     * Вывести ошибку в консоль
     *
     * @param exception - ошибка
     */
    public void exception(Exception exception) throws Exception {
        throw exception;
    }

    /**
     * Вывести сообщение об ошибке в консоль
     *
     * @param message - сообщение
     */
    public void error(String message) {
        logger.log(Level.SEVERE, message);
    }

    /**
     * Вывести какую-то информацию в консоль
     *
     * @param message - сообщение
     */
    public void info(String message) {
        logger.log(Level.INFO, message);
    }

    /**
     * Вывести предупреждение в консоль
     *
     * @param message - сообщение
     */
    public void warn(String message) {
        logger.log(Level.WARNING, message);
    }
}
