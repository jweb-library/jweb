package ru.stonlex.jweb.web.adapter.script;

import lombok.Getter;
import ru.stonlex.jweb.http.JHttpConnection;

@Getter
public class ScriptDocument extends ScriptNode {

    private final String url;

    public ScriptDocument(String url, JHttpConnection httpConnection) {
        super(httpConnection);

        this.url = url;
    }
}
