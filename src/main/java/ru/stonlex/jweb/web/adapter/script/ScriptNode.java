package ru.stonlex.jweb.web.adapter.script;

import lombok.Getter;
import lombok.RequiredArgsConstructor;
import ru.stonlex.jweb.http.JHttpConnection;

@Getter
@RequiredArgsConstructor
public class ScriptNode {

    private final JHttpConnection httpConnection;

    public boolean isConnected() {
        return httpConnection.isConnected();
    }

}
