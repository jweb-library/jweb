package ru.stonlex.jweb.web.adapter.stylesheet;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;

@AllArgsConstructor
@Getter
@Setter
public class StyleType {

    private final String styleName;

    private String styleValue;
}
