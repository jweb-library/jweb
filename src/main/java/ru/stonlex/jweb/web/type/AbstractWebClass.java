package ru.stonlex.jweb.web.type;

import lombok.Getter;
import lombok.RequiredArgsConstructor;

@RequiredArgsConstructor
@Getter
public abstract class AbstractWebClass {

    private final WebClassType classType;
}
