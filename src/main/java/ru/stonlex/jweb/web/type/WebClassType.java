package ru.stonlex.jweb.web.type;

import lombok.Getter;
import lombok.RequiredArgsConstructor;

@Getter
@RequiredArgsConstructor
public enum WebClassType {

    HTML(0),
    STYLESHEET(1),
    SCRIPT(2);

    private final int classId;
}
